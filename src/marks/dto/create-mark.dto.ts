import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateMarkDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  mark: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  studentId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  lectorId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  courseId: number;
}
