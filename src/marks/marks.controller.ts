import { Controller, Get } from '@nestjs/common';
import {
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { MarksService } from './marks.service';

@ApiTags('Marks')
@Controller('api/v1/marks')
export class MarksController {
  constructor(private readonly marksService: MarksService) {}

  @Get()
  @ApiOperation({ summary: 'Get all marks' })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  findAllMarks() {
    return this.marksService.findAllMarks();
  }
}
