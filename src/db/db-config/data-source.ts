import { DataSource } from 'typeorm';
import { dbConfiguration } from './db-config';

export const AppDataSource = new DataSource(dbConfiguration(true));
