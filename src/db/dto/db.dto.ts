import { IsNumberString, IsString, IsNotEmpty, IsEnum } from 'class-validator';
import { AppEnvs } from '../../enums/app-enums';

export class DbDto {
  @IsNotEmpty()
  @IsString()
  @IsEnum(AppEnvs)
  APP_ENV: AppEnvs;

  @IsNotEmpty()
  @IsNumberString()
  SERVER_PORT: number;

  @IsNotEmpty()
  @IsNumberString()
  DATABASE_PORT: number;

  @IsNotEmpty()
  @IsString()
  DATABASE_HOST: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_USER: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_PASSWORD: string;

  @IsNotEmpty()
  @IsString()
  DATABASE_NAME: string;
}
