import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Student } from './entities/student.entity';
import { IStudentQuery, TSortBy } from './students.controller';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
  ) {}

  async createStudent(
    createStudentDto: CreateStudentDto,
  ): Promise<CreateStudentDto & Student> {
    const student: Student | null = await this.studentRepository.findOneBy({
      email: createStudentDto.email,
    });

    if (student) {
      throw new HttpException('Student already exist', HttpStatus.CONFLICT);
    }

    const newStudent: CreateStudentDto & Student =
      await this.studentRepository.save(createStudentDto);
    return newStudent;
  }

  async findAndSortAllStudents(sortBy: TSortBy): Promise<Student[] | []> {
    let orderBy: string;
    let orderLine: string;

    switch (sortBy) {
      case 'all':
        orderBy = 'ASC';
        orderLine = 'id';
        break;
      case 'nameASC':
        orderBy = 'ASC';
        orderLine = 'name';

        break;
      case 'nameDESC':
        orderBy = 'DESC';
        orderLine = 'name';
        break;
      case 'createdAtASC':
        orderBy = 'ASC';
        orderLine = 'createdAt';
        break;
      case 'createdAtDESC':
        orderBy = 'DESC';
        orderLine = 'createdAt';
        break;
      default:
        orderBy = 'ASC';
        orderLine = 'id';
    }

    return await this.studentRepository.find({
      order: {
        [orderLine]: orderBy,
      },
    });
  }

  async findAndSortStudentsByQuery(query: IStudentQuery) {
    const students = await this.findAndSortAllStudents(query.sortBy);

    const searchingStudents = [...students].filter((student) =>
      student.name.toLowerCase().includes(query.name.toLowerCase()),
    );

    return searchingStudents;
  }

  async findOneStudentById(id: number): Promise<Student> {
    const student: Student | undefined = await this.studentRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as imagePath',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.id = :id', { id })
      .leftJoinAndSelect('student.courses', 'course')
      .getRawOne();

    if (!student) {
      throw new HttpException('Student not found', HttpStatus.NOT_FOUND);
    }

    return student;
  }

  async updateStudent(
    id: number,
    updateStudentDto: UpdateStudentDto,
  ): Promise<string> {
    const res: UpdateResult = await this.studentRepository
      .createQueryBuilder()
      .update(Student)
      .set(updateStudentDto)
      .where('id = :id', { id })
      .execute();

    if (!res.affected) {
      throw new HttpException('Student not found', HttpStatus.NOT_FOUND);
    }
    return 'Successfully updated';
  }

  async removeStudent(id: number): Promise<string> {
    const res: DeleteResult = await this.studentRepository.delete(id);

    if (!res.affected) {
      throw new HttpException('Student not found', HttpStatus.NOT_FOUND);
    }
    return 'Successfully deleted';
  }
}
