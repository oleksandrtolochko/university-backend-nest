import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  UseInterceptors,
  UploadedFile,
  Res,
  Query,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { StudentsService } from './students.service';
import {
  CreateStudentDto,
  UpdateStudentDto,
  ResGetAllStudentsDto,
  ResGetStudentByIdDto,
} from './dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Response } from 'express';

export type TSortBy =
  | 'nameASC'
  | 'nameDESC'
  | 'createdAtASC'
  | 'createdAtDESC'
  | 'all';
export interface IStudentQuery {
  name?: string | undefined;
  sortBy: TSortBy;
}

@ApiTags('Students')
@Controller('api/v1/students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @Post()
  @ApiOperation({ summary: 'Create new Student' })
  @ApiCreatedResponse({
    description: 'Successfully created',
    type: CreateStudentDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: [
            'name | surname | email  must be a string',
            'age must be a number',
            'name | surname | email | age should not be empty',
          ],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiConflictResponse({
    description: 'Student already exist',
    content: {
      'text/html': {
        example: 'Student already exist',
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  async createStudent(@Body() createStudentDto: CreateStudentDto) {
    return await this.studentsService.createStudent(createStudentDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get all students' })
  @ApiOkResponse({
    description:
      "Get array of all students or empty array if student weren't found",
    type: [ResGetAllStudentsDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  async findAllStudents(@Query() query: IStudentQuery) {
    if (query.name) {
      return this.studentsService.findAndSortStudentsByQuery(query);
    }
    return await this.studentsService.findAndSortAllStudents(query.sortBy);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get student by ID' })
  @ApiOkResponse({
    description: 'Student with searching ID',
    type: ResGetStudentByIdDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Student not Found',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  async findOneStudentById(@Param('id', ParseIntPipe) id: string) {
    return await this.studentsService.findOneStudentById(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update the student' })
  @ApiOkResponse({
    description: 'Message about successful student updating',
    content: {
      'text/html': {
        example: 'Successfully updated',
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Student not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  async updateStudent(
    @Param('id', ParseIntPipe) id: string,
    @Body() updateStudentDto: UpdateStudentDto,
  ) {
    return await this.studentsService.updateStudent(+id, updateStudentDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete the student' })
  @ApiOkResponse({
    description: 'Message about success student deleting',
    content: {
      'text/html': {
        example: 'Successfully deleted',
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Student not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  async removeStudent(@Param('id', ParseIntPipe) id: string) {
    return await this.studentsService.removeStudent(+id);
  }

  @Post(':id/upload-photo')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './upload',
        filename: (req, file, cb) => {
          const name = file.originalname.split('.')[0];
          console.log('name: ', name);
          const fileExtension = file.originalname.split('.')[1];
          const newFileName =
            name.split(' ').join('_') + '_' + Date.now() + '.' + fileExtension;
          console.log('newFileName: ', newFileName);
          cb(null, newFileName);
        },
      }),
      fileFilter: (req, file, cb) => {
        console.log('file: ', file);
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/i)) {
          return cb(null, false);
        }
        if (file.size > 10000000) {
          return cb(null, false);
        }
        cb(null, true);
      },
    }),
  )
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Param('id', ParseIntPipe) id: string,
  ) {
    console.log(file);
    await this.studentsService.updateStudent(+id, {
      imagePath: file.path,
    });
    return file.filename;
  }

  @Get('/photo/:filename')
  async getPhoto(@Param('filename') filename: string, @Res() res: Response) {
    res.sendFile(filename, { root: './upload' });
  }
}
