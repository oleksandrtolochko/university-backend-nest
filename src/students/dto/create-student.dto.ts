import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateStudentDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  surname: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ type: String })
  email: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  age: number;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ type: String })
  imagePath?: string;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ type: Number })
  groupId?: number;
}
