import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResGetStudentByIdDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The creation date and time in ISO 8601 format.',
  })
  createdAt: string;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The updating date and time in ISO 8601 format.',
  })
  updatedAt: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    type: String,
    description: 'Student name',
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    type: String,
    description: 'Student surname',
  })
  surname: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    type: String,
    description: 'Student email',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    type: String,
    description: 'Student age',
  })
  age: '20';

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    type: String,
    description: 'Student group name',
  })
  groupName: string;
}
