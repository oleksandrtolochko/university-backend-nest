import { CreateStudentDto } from './create-student.dto';
import { UpdateStudentDto } from './update-student.dto';
import { ResGetAllStudentsDto } from './res-get-all-students.dto';
import { ResGetStudentByIdDto } from './res-get-student-by-id.dto';

export {
  CreateStudentDto,
  UpdateStudentDto,
  ResGetAllStudentsDto,
  ResGetStudentByIdDto,
};
