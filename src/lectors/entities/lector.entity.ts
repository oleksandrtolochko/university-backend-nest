import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { CoreEntity } from '../../entities/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ResetToken } from '../../reset-token/entities/reset-token.entity';
import { Exclude } from 'class-transformer';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: true,
  })
  name?: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  surname?: string;

  @Column({
    type: 'varchar',
  })
  email: string;

  @Column({
    type: 'varchar',
  })
  @Exclude()
  password: string;

  @ManyToMany(() => Course, (course) => course.lectors, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'lector_course',
    joinColumn: {
      name: 'lector_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  marks: Mark[];

  @OneToOne(() => ResetToken, (resetToken) => resetToken.lector, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'resetToken_id' })
  resetToken: ResetToken;
}
