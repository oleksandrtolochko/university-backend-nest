import { CreateLectorDto } from './create-lector.dto';
import { ResCreateLectorDto } from './res-create-lector-dto';
import { UpdateLectorDto } from './update-lector.dto';
import { ResGetAllLectorsDto } from './res-get-all-lectors.dto';
import { ResGetLectorDto } from './res-get-lector.dto';

export {
  CreateLectorDto,
  ResCreateLectorDto,
  UpdateLectorDto,
  ResGetAllLectorsDto,
  ResGetLectorDto,
};
