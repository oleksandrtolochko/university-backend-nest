import {
  IsArray,
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ResGetCourseDto } from '../../courses/dto';

export class ResGetLectorDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, description: 'Lector name' })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, description: 'Lector surname' })
  surname: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ type: String, description: 'Lector email' })
  email: string;

  // @Exclude()
  // @IsNotEmpty()
  // @IsString()
  // @ApiProperty({ type: String, description: 'Password' })
  // password: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number, description: 'Lector ID' })
  id: number;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The creation date and time in ISO 8601 format.',
  })
  createdAt: string;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The updating date and time in ISO 8601 format.',
  })
  updatedAt: string;

  @IsArray()
  @ApiProperty({
    type: [ResGetCourseDto],
    description: 'Array of lector courses or an empty array',
  })
  courses: [];
}
