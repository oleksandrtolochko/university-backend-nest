import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

export class ResCreateLectorDto {
  @IsString()
  @ApiProperty({ type: String, description: 'Lector name' })
  name?: string;

  @IsString()
  @ApiProperty({ type: String, description: 'Lector surname' })
  surname?: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ type: String, description: 'Lector email' })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, description: 'Password' })
  @Exclude()
  password: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number, description: 'Lector ID' })
  id: number;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The creation date and time in ISO 8601 format.',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The updating date and time in ISO 8601 format.',
  })
  updatedAt: Date;
}
