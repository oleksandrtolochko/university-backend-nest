import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateLectorDto {
  @IsString()
  @ApiProperty({ type: String })
  name?: string;

  @IsString()
  @ApiProperty({ type: String })
  surname?: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  password: string;
}
