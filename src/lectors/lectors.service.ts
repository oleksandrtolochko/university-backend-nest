import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateLectorDto } from './dto/create-lector.dto';
import { Lector } from './entities/lector.entity';
import { ResCreateLectorDto, UpdateLectorDto } from './dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
  ) {}

  async createLector(
    createLectorDto: CreateLectorDto,
  ): Promise<ResCreateLectorDto> {
    const lector: Lector | null = await this.lectorRepository.findOneBy({
      email: createLectorDto.email,
    });

    if (lector) {
      throw new HttpException('Lector already exist', HttpStatus.CONFLICT);
    }

    createLectorDto.password = await bcrypt.hash(createLectorDto.password, 10);

    const newLector: ResCreateLectorDto =
      await this.lectorRepository.save(createLectorDto);

    return newLector;
  }

  async findAllLectors(): Promise<Lector[]> {
    const lectors: Lector[] = await this.lectorRepository.find({});
    return lectors;
  }

  async findOneLectorByEmail(email: string): Promise<Lector> {
    // const lector: Lector | null = await this.lectorRepository.findOneBy({
    //   email,
    // });

    // if (!lector) {
    //   throw new HttpException('Lector not found', HttpStatus.NOT_FOUND);
    // }
    // return lector;

    const lector: Lector | null = await this.lectorRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.resetToken', 'resetToken')
      .where('lector.email = :email', { email })
      .getOne();

    if (!lector) {
      throw new HttpException('Lector not found', HttpStatus.NOT_FOUND);
    }

    return lector;
  }

  async findOneLectorById(id: number): Promise<Lector> {
    const lector: Lector | null = await this.lectorRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.courses', 'course')
      .leftJoinAndSelect('course.students', 'student')
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new HttpException('Lector not found', HttpStatus.NOT_FOUND);
    }

    return lector;
  }

  async findLectorCourses(id: number) {
    const lector: Lector | null = await this.lectorRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.courses', 'course')
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new HttpException('Lector not found', HttpStatus.NOT_FOUND);
    }

    if (!lector.courses.length) {
      return null;
    }
    return lector;
  }

  async removeLector(id: number) {
    const res: DeleteResult = await this.lectorRepository.delete(id);

    if (!res.affected) {
      throw new HttpException('Lector not found', HttpStatus.NOT_FOUND);
    }
    return 'Successfully deleted';
  }

  async addLectorToCourse(lectorId: number, courseId: number) {
    try {
      await this.lectorRepository
        .createQueryBuilder()
        .relation(Lector, 'courses')
        .of(lectorId)
        .add(courseId);
    } catch (error) {
      throw new HttpException(`${error}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async updateLector(id: number, updateLectorDto: UpdateLectorDto) {
    const res: UpdateResult = await this.lectorRepository.update(
      id,
      updateLectorDto,
    );

    if (!res.affected) {
      throw new HttpException(
        'Someting went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    return 'Successfully updated';
  }
}
