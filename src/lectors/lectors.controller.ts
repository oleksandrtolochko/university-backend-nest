import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  ParseIntPipe,
  UseInterceptors,
  ClassSerializerInterceptor,
  Patch,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { LectorsService } from './lectors.service';
import {
  CreateLectorDto,
  ResCreateLectorDto,
  ResGetAllLectorsDto,
  ResGetLectorDto,
  UpdateLectorDto,
} from './dto';

@ApiTags('Lectors')
@Controller('api/v1/lectors')
@UseInterceptors(ClassSerializerInterceptor)
export class LectorsController {
  constructor(private readonly lectorsService: LectorsService) {}

  @Post()
  @ApiOperation({ summary: 'Create new Lector' })
  @ApiCreatedResponse({
    description: 'Successfully created',
    type: ResCreateLectorDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: [
            'name | email | password must be a string',
            'name | email | password should not be empty',
          ],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiConflictResponse({
    description: 'Lector already exist',
    content: {
      'text/html': {
        example: 'Lector already exist',
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  createLector(@Body() createLectorDto: CreateLectorDto) {
    return this.lectorsService.createLector(createLectorDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get all lectors' })
  @ApiOkResponse({
    description:
      "Get array of all lectors or empty array if lectors weren't found",
    type: [ResGetAllLectorsDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  findAllLectors() {
    return this.lectorsService.findAllLectors();
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get lector by ID' })
  @ApiOkResponse({
    description: 'Lector with searching ID',
    type: ResGetLectorDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Lector not Found',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  findOneLectorById(@Param('id', ParseIntPipe) id: string) {
    return this.lectorsService.findOneLectorById(+id);
  }

  @Get(':id/courses')
  @ApiOperation({ summary: "Get lector's courses" })
  @ApiOkResponse({
    description: 'Lector courses',
    type: ResGetLectorDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Lector not Found',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  findLectorCourses(@Param('id', ParseIntPipe) id: string) {
    return this.lectorsService.findLectorCourses(+id);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete lector' })
  @ApiOkResponse({
    description: 'Message about success lector deleting',
    content: {
      'text/html': {
        example: 'Successfully deleted',
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Lector not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  removeLector(@Param('id', ParseIntPipe) id: string) {
    return this.lectorsService.removeLector(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update lector by ID' })
  @ApiOkResponse({
    description: 'Message about successful updating',
    content: {
      'text/html': {
        example: 'Successfully updated',
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  updateLector(
    @Param('id', ParseIntPipe) id: string,
    @Body() updateLectorDto: UpdateLectorDto,
  ) {
    return this.lectorsService.updateLector(+id, updateLectorDto);
  }
}
