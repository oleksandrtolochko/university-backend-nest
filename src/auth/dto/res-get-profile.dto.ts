import { IsDate, IsEmail, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ProfileResponseDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  sub: number;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  username: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  iat: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  exp: Date;
}
