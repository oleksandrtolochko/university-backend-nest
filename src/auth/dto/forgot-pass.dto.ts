import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ForgotPassDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;
}
