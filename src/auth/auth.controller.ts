import {
  Controller,
  Post,
  Body,
  Get,
  Request,
  //   Param,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import {
  SignRequestDto,
  SignResponseDto,
  ProfileResponseDto,
  ForgotPassDto,
  ResetPasswordWithTokenRequestDto,
} from './dto';
import { Public } from './guard/public.decorator';
import {
  ApiConflictResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiTags,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiUnauthorizedResponse,
  ApiOkResponse,
  ApiBadRequestResponse,
} from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('api/v1/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Sign in' })
  @Public()
  @Post('sign-in')
  @ApiOkResponse({
    description: 'Sign in successfull',
    content: {
      'application/json': {
        example: {
          statusCode: 200,
          accessToken:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoieWFiaWM1NjA3OEB3bG15Y24uY29tIiwiaWF0IjoxNjkzNjg0OTM1LCJleHAiOjE2OTM2ODUwNTV9.3t60tiLbnuFkDWKx2d4FK8tdlNEA0qAnOdSxRariKfw',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: [
            'email | password must be a string',
            'email | password should not be empty',
          ],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'User not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  public signIn(@Body() signInDto: SignRequestDto): Promise<SignResponseDto> {
    return this.authService.signIn(signInDto.email, signInDto.password);
  }

  @ApiOperation({ summary: 'Sign up' })
  @Public()
  @Post('sign-up')
  @ApiCreatedResponse({
    description: 'Successfully created',
    content: {
      'text/html': {
        example: 'Successfully registered. Now you can sign in to the app',
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: [
            'email | password must be a string',
            'email | password should not be empty',
          ],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiConflictResponse({
    description: 'Lector already exist',
    content: {
      'text/html': {
        example: 'Lector already exist',
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  public signUp(@Body() signUpDto: SignRequestDto): Promise<string> {
    return this.authService.signUp(signUpDto.email, signUpDto.password);
  }

  @ApiOperation({ summary: 'Request to change password' })
  @Public()
  @Post('forgot-password')
  @ApiCreatedResponse({
    description: 'Sign in successfull',
    content: {
      'application/json': {
        example: {
          statusCode: 201,
          message:
            'Token successfully created check your email. You have 5 minutes for resetting your password',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: ['email must be a string', 'email should not be empty'],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'User not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  public forgotPass(@Body() forgotPassDto: ForgotPassDto) {
    return this.authService.requestForResetPassword(forgotPassDto);
  }

  @ApiOperation({ summary: 'Changing password' })
  @Public()
  @Post('reset-password')
  @ApiOkResponse({
    description: 'Message about successful password updating',
    content: {
      'text/html': {
        example: 'Successfully updated',
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: [
            'password must be a string',
            'password should not be empty',
          ],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Invalid token',
    content: {
      'application/json': {
        example: {
          statusCode: 400,
          message: 'Invalid token',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: "Reset token doesn't exist",
    content: {
      'application/json': {
        example: {
          statusCode: 400,
          message: 'There is no request to change password for this user.',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  public resetPassword(
    @Body() resetPasswordDto: ResetPasswordWithTokenRequestDto,
  ) {
    return this.authService.resetPassword(resetPasswordDto);
  }

  @ApiOperation({ summary: 'Get profile' })
  @Get('profile')
  @ApiOkResponse({
    description: 'Information about current user',
    content: {
      'application/json': {
        example: {
          sub: 2,
          username: 'yabic56078@wlmycn.com',
          iat: 1693688444,
          exp: 1693688564,
        },
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  getProfile(@Request() req): Promise<ProfileResponseDto> {
    return req.user;
  }
  //   @Public()
  //   @Get('reset-password/:token')
  //   public getResetPass(@Param('token') token: string) {
  //     return this.authService.checkResetToken(token);
  //   }
}
