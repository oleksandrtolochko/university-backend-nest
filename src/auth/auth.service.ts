import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { LectorsService } from '../lectors/lectors.service';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { ForgotPassDto, ResetPasswordWithTokenRequestDto } from './dto';

@Injectable()
export class AuthService {
  constructor(
    private lectorsService: LectorsService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
  ) {}

  public async signUp(email: string, password: string): Promise<string> {
    const user = await this.lectorsService.createLector({ email, password });

    if (!user) {
      throw new HttpException(
        'Something went wrong. Try again.',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    return 'Successfully registered. Now you can sign in to the app';
  }

  public async signIn(
    email: string,
    pass: string,
  ): Promise<{
    accessToken: string;
  }> {
    const user = await this.lectorsService.findOneLectorByEmail(email);

    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    const match = await bcrypt.compare(pass, user.password);

    if (!match) {
      throw new UnauthorizedException();
    }
    const payload = { sub: user.id, username: user.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async requestForResetPassword(forgotPassDto: ForgotPassDto) {
    const user = await this.lectorsService.findOneLectorByEmail(
      forgotPassDto.email,
    );

    if (!user) {
      throw new HttpException('User not found!', HttpStatus.NOT_FOUND);
    }

    if (user.resetToken) {
      user.resetToken = null;
    }

    return this.resetTokenService.generateResetToken(user);
  }

  public async resetPassword(
    resetPasswordDto: ResetPasswordWithTokenRequestDto,
  ) {
    return this.resetTokenService.resetPassword(resetPasswordDto);
  }

  //   public async checkResetToken(token: string) {
  //     return this.resetTokenService.checkResetToken(token);
  //   }
}
