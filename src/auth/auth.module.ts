import 'dotenv/config';
import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './guard/auth.guard';
import { LectorsModule } from '../lectors/lectors.module';
import { ResetTokenModule } from '../reset-token/reset-token.module';

@Module({
  imports: [
    LectorsModule,
    JwtModule.register({
      global: true,
      secret: process.env.SECRET_JWT,

      // signOptions: { expiresIn: '120s' },
    }),
    ResetTokenModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AuthModule {}
