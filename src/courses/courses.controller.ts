import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  ParseIntPipe,
  Patch,
  Delete,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { ResCreateCourseDto, ResGetCourseDto, UpdateCourseDto } from './dto';

@ApiTags('Courses')
@Controller('api/v1/courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @Post()
  @ApiOperation({ summary: 'Create new Course' })
  @ApiCreatedResponse({
    description: 'Successfully created',
    type: ResCreateCourseDto,
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: [
            'name | description must be a string',
            'hours must be a number',
            'name | description | hours should not be empty',
          ],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiConflictResponse({
    description: 'Course already exist',
    content: {
      'text/html': {
        example: 'Course already exist',
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  createCourse(@Body() createCourseDto: CreateCourseDto) {
    return this.coursesService.createCourse(createCourseDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get courses list' })
  @ApiOkResponse({
    description: "Get courses list or empty array if course weren't found",
    type: [ResGetCourseDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  findAllCourses() {
    return this.coursesService.findAllCourses();
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get course by ID' })
  @ApiOkResponse({
    description: 'Get info about some Course with ID',
    type: ResGetCourseDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Course not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  findCourseById(@Param('id', ParseIntPipe) id: string) {
    return this.coursesService.findCourseById(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update Course' })
  @ApiOkResponse({
    description: 'Message about succesfully apdated Course',
    content: {
      'html/text': {
        example: {
          statusCode: 200,
          message: 'Successfully updated',
        },
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Course not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  updateCourse(
    @Param('id', ParseIntPipe) id: string,
    @Body() updateCourseDto: UpdateCourseDto,
  ) {
    return this.coursesService.updateCourse(+id, updateCourseDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete Course' })
  @ApiOkResponse({
    description: 'Message about succesfully deleted Course',
    content: {
      'html/text': {
        example: {
          statusCode: 200,
          message: 'Successfully deleted',
        },
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Course not Found',
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  deleteCourse(@Param('id') id: string) {
    return this.coursesService.removeCourse(+id);
  }
}
