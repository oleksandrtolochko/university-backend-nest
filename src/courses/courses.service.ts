import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { Course } from './entities/course.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { UpdateCourseDto } from './dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
  ) {}

  async createCourse(
    createCourseDto: CreateCourseDto,
  ): Promise<CreateCourseDto & Course> {
    const course: Course | null = await this.courseRepository.findOneBy({
      name: createCourseDto.name,
    });

    if (course) {
      throw new HttpException('Course already exist', HttpStatus.CONFLICT);
    }

    const newCourse: CreateCourseDto & Course =
      await this.courseRepository.save(createCourseDto);
    return newCourse;
  }

  async findAllCourses(): Promise<Course[]> {
    const courses = await this.courseRepository.find({
      relations: ['students'],
    });
    return courses;
  }

  async findCourseById(id: number): Promise<Course | null> {
    const course: Course | null = await this.courseRepository.findOneBy({ id });

    if (!course) {
      throw new HttpException('Course not found', HttpStatus.NOT_FOUND);
    }
    return course;
  }

  async removeCourse(id: number) {
    const res: DeleteResult = await this.courseRepository.delete(id);
    if (!res.affected) {
      throw new HttpException('Course not found', HttpStatus.NOT_FOUND);
    }
    return 'Successfully deleted';
  }

  async updateCourse(
    id: number,
    updateCourseDto: UpdateCourseDto,
  ): Promise<string> {
    const res: UpdateResult = await this.courseRepository.update(
      id,
      updateCourseDto,
    );
    if (!res.affected) {
      throw new HttpException('Course not found', HttpStatus.NOT_FOUND);
    }
    return 'Successfully updated';
  }
}
