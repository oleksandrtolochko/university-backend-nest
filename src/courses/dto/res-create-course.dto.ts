import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResCreateCourseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, description: 'Course name' })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, description: 'Course description' })
  description: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number, description: 'Course hours' })
  hours: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number, description: 'Course ID' })
  id: number;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The creation date and time in ISO 8601 format.',
  })
  createdAt: string;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The updating date and time in ISO 8601 format.',
  })
  updatedAt: string;
}
