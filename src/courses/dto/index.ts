import { CreateCourseDto } from './create-course.dto';
import { ResGetCourseDto } from './res-get-course.dto';
import { UpdateCourseDto } from './update-course.dto';
import { ResCreateCourseDto } from './res-create-course.dto';

export {
  CreateCourseDto,
  ResGetCourseDto,
  UpdateCourseDto,
  ResCreateCourseDto,
};
