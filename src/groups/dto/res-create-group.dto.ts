import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResCreateGroupDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, description: 'Group name' })
  name: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number, description: 'Group ID' })
  id: number;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The creation date and time in ISO 8601 format.',
  })
  createdAt: string;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty({
    type: String,
    description: 'The updating date and time in ISO 8601 format.',
  })
  updatedAt: string;
}
