import { Column, Entity, OneToMany } from 'typeorm';
import { CoreEntity } from '../../entities/core.entity';
import { Student } from '../../students/entities/student.entity';

@Entity({ name: 'groups' })
export class Group extends CoreEntity {
  @Column({
    type: 'varchar',
  })
  name: string;

  @OneToMany(() => Student, (student) => student.group, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  students: Student[];
}
