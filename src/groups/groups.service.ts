import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { Group } from './entities/group.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupRepository: Repository<Group>,
  ) {}

  async createGroup(
    createGroupDto: CreateGroupDto,
  ): Promise<CreateGroupDto & Group> {
    const group: Group | null = await this.groupRepository.findOneBy({
      name: createGroupDto.name,
    });

    if (group) {
      throw new HttpException('Group already exist', HttpStatus.BAD_REQUEST);
    }

    const newGroup: CreateGroupDto & Group =
      await this.groupRepository.save(createGroupDto);
    return newGroup;
  }

  async findAllGroups(): Promise<Group[]> {
    const groups: Group[] = await this.groupRepository.find({
      relations: ['students'],
      order: {
        id: 'ASC',
      },
    });

    return groups;
  }

  async findOneGroupById(id: number): Promise<Group> {
    const group: Group | null = await this.groupRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'student')
      .where('group.id = :id', { id })
      .getOne();

    if (!group) {
      throw new HttpException('Group not found', HttpStatus.NOT_FOUND);
    }

    return group;
  }

  async updateGroupById(
    id: number,
    updateGroupDto: UpdateGroupDto,
  ): Promise<string> {
    const res: UpdateResult = await this.groupRepository
      .createQueryBuilder()
      .update(Group)
      .set(updateGroupDto)
      .where('id = :id', { id })
      .execute();

    if (!res.affected) {
      throw new HttpException('Group not found', HttpStatus.NOT_FOUND);
    }
    return 'Successfully updated';
  }

  async removeGroup(id: number): Promise<string> {
    const res: DeleteResult = await this.groupRepository.delete(id);
    if (!res.affected) {
      throw new HttpException('Group not found', HttpStatus.NOT_FOUND);
    }
    return 'Successfully deleted';
  }
}
