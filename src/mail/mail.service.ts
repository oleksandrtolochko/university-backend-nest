import 'dotenv/config';
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserConfirmation(email: string, token: string) {
    const url = `http://localhost:3000/reset-password?token=${token}`;

    await this.mailerService.sendMail({
      to: email,
      from: process.env.SENDER_EMAIL,
      subject:
        'Welcome to reset password service! Confirm your Email and continue',
      template: './reset-pass-letter',
      context: {
        url,
      },
    });
  }
}
