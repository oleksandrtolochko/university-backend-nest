import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentsModule } from './students/students.module';
import { DbModule } from './db/db.module';
import { typeOrmAsyncConfig } from './db/db-config/typeorm-config';
import { GroupsModule } from './groups/groups.module';
import { CoursesModule } from './courses/courses.module';
import { LectorsModule } from './lectors/lectors.module';
import { LectorCourseModule } from './lector-course/lector-course.module';
import { MarksModule } from './marks/marks.module';
import { MarksManageModule } from './marks-manage/marks-manage.module';
import { AuthModule } from './auth/auth.module';
import { ResetTokenModule } from './reset-token/reset-token.module';
import { MailModule } from './mail/mail.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    DbModule,
    StudentsModule,
    GroupsModule,
    CoursesModule,
    LectorsModule,
    LectorCourseModule,
    MarksModule,
    MarksManageModule,
    AuthModule,
    ResetTokenModule,
    MailModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
