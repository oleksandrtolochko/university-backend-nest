import { Controller, Body, Patch, Param, ParseIntPipe } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { LectorCourseService } from './lector-course.service';
import { UpdateLectorCourseDto } from './dto/update-lector-course.dto';

@ApiTags('Lectors')
@Controller('api/v1/lectors')
export class LectorCourseController {
  constructor(private readonly lectorCourseService: LectorCourseService) {}

  @Patch(':id/course')
  @ApiOperation({ summary: 'Add lector to the course' })
  @ApiOkResponse({
    description: 'Lector courses',
    content: {
      'text/html': {
        example: 'Lector successfully added to the course',
      },
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Lector | Course not Found',
        },
      },
    },
  })
  @ApiConflictResponse({
    description: 'Conflict',
    content: {
      'application/json': {
        example: {
          message: 'Lector already at this course',
          statusCode: 409,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  addLectorToCourse(
    @Param('id', ParseIntPipe) id: string,
    @Body() updateLectorCourseDto: UpdateLectorCourseDto,
  ) {
    return this.lectorCourseService.addLectorToCourse(
      +id,
      updateLectorCourseDto.id,
    );
  }
}
