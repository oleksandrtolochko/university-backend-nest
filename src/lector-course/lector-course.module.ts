import { Module } from '@nestjs/common';
import { LectorCourseService } from './lector-course.service';
import { LectorCourseController } from './lector-course.controller';
import { CoursesModule } from 'src/courses/courses.module';
import { LectorsModule } from 'src/lectors/lectors.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lector } from 'src/lectors/entities/lector.entity';

@Module({
  imports: [LectorsModule, CoursesModule, TypeOrmModule.forFeature([Lector])],
  controllers: [LectorCourseController],
  providers: [LectorCourseService],
  exports: [LectorCourseService],
})
export class LectorCourseModule {}
