import { Test, TestingModule } from '@nestjs/testing';
import { LectorCourseController } from './lector-course.controller';
import { LectorCourseService } from './lector-course.service';

describe('LectorCourseController', () => {
  let controller: LectorCourseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LectorCourseController],
      providers: [LectorCourseService],
    }).compile();

    controller = module.get<LectorCourseController>(LectorCourseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
