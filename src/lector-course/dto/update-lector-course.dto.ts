import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateLectorCourseDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  id: number;
}
