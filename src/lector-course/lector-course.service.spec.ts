import { Test, TestingModule } from '@nestjs/testing';
import { LectorCourseService } from './lector-course.service';

describe('LectorCourseService', () => {
  let service: LectorCourseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LectorCourseService],
    }).compile();

    service = module.get<LectorCourseService>(LectorCourseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
