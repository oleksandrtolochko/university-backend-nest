import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CoursesService } from 'src/courses/courses.service';
import { Course } from 'src/courses/entities/course.entity';
import { Lector } from 'src/lectors/entities/lector.entity';
import { LectorsService } from 'src/lectors/lectors.service';

@Injectable()
export class LectorCourseService {
  constructor(
    private readonly lectorsService: LectorsService,
    private readonly coursesService: CoursesService,
  ) {}

  async addLectorToCourse(lectorId: number, courseId: number) {
    const course: Course | null =
      await this.coursesService.findCourseById(courseId);

    if (!course) {
      throw new HttpException("Course doesn't exist", HttpStatus.BAD_REQUEST);
    }

    const lector: Lector | null =
      await this.lectorsService.findOneLectorById(lectorId);

    if (!lector) {
      throw new HttpException("Lector doesn't exist", HttpStatus.BAD_REQUEST);
    }

    const lectorCourseCheck: Course[] = lector.courses.filter(
      (course) => course.id === courseId,
    );

    if (lectorCourseCheck.length) {
      throw new HttpException(
        'Lector already at this course',
        HttpStatus.CONFLICT,
      );
    }
    await this.lectorsService.addLectorToCourse(lectorId, courseId);
    return 'Lector successfully added to the course';
  }
}
