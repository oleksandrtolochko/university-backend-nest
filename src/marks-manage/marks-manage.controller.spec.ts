import { Test, TestingModule } from '@nestjs/testing';
import { MarksManageController } from './marks-manage.controller';
import { MarksManageService } from './marks-manage.service';

describe('MarksManageController', () => {
  let controller: MarksManageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MarksManageController],
      providers: [MarksManageService],
    }).compile();

    controller = module.get<MarksManageController>(MarksManageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
