import { Module } from '@nestjs/common';
import { MarksManageService } from './marks-manage.service';
import { MarksManageController } from './marks-manage.controller';
import { LectorsModule } from 'src/lectors/lectors.module';
import { StudentsModule } from 'src/students/students.module';
import { MarksModule } from 'src/marks/marks.module';
import { CoursesModule } from 'src/courses/courses.module';
import { Mark } from 'src/marks/entities/mark.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    LectorsModule,
    StudentsModule,
    MarksModule,
    CoursesModule,
    TypeOrmModule.forFeature([Mark]),
  ],
  controllers: [MarksManageController],
  providers: [MarksManageService],
  exports: [MarksManageService],
})
export class MarksManageModule {}
