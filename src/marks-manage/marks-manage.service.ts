import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMarksManageDto } from './dto/create-marks-manage.dto';
import { Mark } from 'src/marks/entities/mark.entity';
import { CoursesService } from '../courses/courses.service';
import { LectorsService } from '../lectors/lectors.service';
import { StudentsService } from '../students/students.service';
import { Course } from '../courses/entities/course.entity';
import { Student } from '../students/entities/student.entity';
import { Lector } from '../lectors/entities/lector.entity';

@Injectable()
export class MarksManageService {
  constructor(
    private readonly coursesService: CoursesService,
    private readonly lectorsService: LectorsService,
    private readonly studentsService: StudentsService,

    @InjectRepository(Mark)
    private readonly markRepository: Repository<Mark>,
  ) {}
  async createMark(createMarksManageDto: CreateMarksManageDto) {
    const course: Course | null = await this.coursesService.findCourseById(
      createMarksManageDto.courseId,
    );

    if (!course) {
      throw new HttpException('Course not found', HttpStatus.NOT_FOUND);
    }

    const student: Student | null =
      await this.studentsService.findOneStudentById(
        createMarksManageDto.studentId,
      );

    if (!student) {
      throw new HttpException('Student not found', HttpStatus.NOT_FOUND);
    }

    const lector: Lector | null = await this.lectorsService.findOneLectorById(
      createMarksManageDto.lectorId,
    );
    if (!lector) {
      throw new HttpException('Lector not found', HttpStatus.NOT_FOUND);
    }

    const newMark: Mark = new Mark();
    newMark.course = course;
    newMark.lector = lector;
    newMark.student = student;
    newMark.mark = createMarksManageDto.mark;

    const res = await this.markRepository.save(newMark);
    return res;
  }

  async findMarksByStudentId(id: number) {
    const student: Student | null =
      await this.studentsService.findOneStudentById(id);

    if (!student) {
      throw new HttpException('Student not found', HttpStatus.NOT_FOUND);
    }
    const marks: Mark[] = await this.markRepository
      .createQueryBuilder('mark')
      .select(['course.name AS courseName', 'mark.mark AS mark'])
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.student', 'student')
      .where('student.id = :id', { id })
      .getRawMany();

    return marks;
  }

  async findMarksByCourseId(id: number) {
    const course = await this.coursesService.findCourseById(id);

    if (!course) {
      throw new HttpException('Course not found', HttpStatus.NOT_FOUND);
    }

    const marks: Mark[] = await this.markRepository
      .createQueryBuilder('mark')
      .select([
        'course.name AS courseName',
        'lector.name AS lectorName',
        'student.name AS studentName',
        'student.surname AS studentSurname',
        'mark.mark AS mark',
      ])
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.lector', 'lector')
      .leftJoin('mark.student', 'student')
      .where('course.id = :id', { id })
      .getRawMany();

    return marks;
  }
}
