import { Test, TestingModule } from '@nestjs/testing';
import { MarksManageService } from './marks-manage.service';

describe('MarksManageService', () => {
  let service: MarksManageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MarksManageService],
    }).compile();

    service = module.get<MarksManageService>(MarksManageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
