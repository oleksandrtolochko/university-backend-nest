import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { MarksManageService } from './marks-manage.service';
import {
  CreateMarksManageDto,
  ResGetCoursetMarksDto,
  ResGetStudentMarksDto,
} from './dto';

@ApiTags('Marks')
@Controller('api/v1/marks')
export class MarksManageController {
  constructor(private readonly marksManageService: MarksManageService) {}

  @Post()
  @ApiOperation({ summary: 'Create new Mark' })
  @ApiCreatedResponse({
    description: 'Successfully created',
    type: CreateMarksManageDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: [
            'mark | studentID | lectorId | courseId must be a number conforming to the specified constraints',
            'mark | studentID | lectorId | courseId should not be empty',
          ],
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  createMark(@Body() createMarksManageDto: CreateMarksManageDto) {
    return this.marksManageService.createMark(createMarksManageDto);
  }

  @Get('/students/:id')
  @ApiOperation({ summary: 'Get marks by student ID' })
  @ApiOkResponse({
    description: 'Array of marks with course name',
    type: [ResGetStudentMarksDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Student not Found',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  findMarksByStudentId(@Param('id', ParseIntPipe) id: string) {
    return this.marksManageService.findMarksByStudentId(+id);
  }

  @Get('/courses/:id')
  @ApiOperation({ summary: 'Get marks by course ID' })
  @ApiOkResponse({
    description: 'Array of marks with course name',
    type: [ResGetCoursetMarksDto],
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
    content: {
      'application/json': {
        example: {
          statusCode: 401,
          message: 'Unauthorized',
        },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Not Found',
    content: {
      'application/json': {
        example: {
          statusCode: 404,
          message: 'Course not Found',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
    content: {
      'application/json': {
        example: {
          message: 'Validation failed (numeric string is expected)',
          error: 'Bad Request',
          statusCode: 400,
        },
      },
    },
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
    content: {
      'application/json': {
        example: {
          statusCode: 500,
          message: 'Internal Server Error',
        },
      },
    },
  })
  findMarksByCourseId(@Param('id', ParseIntPipe) id: string) {
    return this.marksManageService.findMarksByCourseId(+id);
  }
}
