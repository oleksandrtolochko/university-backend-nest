import { CreateMarksManageDto } from './create-marks-manage.dto';
import { UpdateMarksManageDto } from './update-marks-manage.dto';
import { ResGetStudentMarksDto } from './res-get-student-marks.dto';
import { ResGetCoursetMarksDto } from './res-get-course-marks.dto';

export {
  CreateMarksManageDto,
  UpdateMarksManageDto,
  ResGetStudentMarksDto,
  ResGetCoursetMarksDto,
};
