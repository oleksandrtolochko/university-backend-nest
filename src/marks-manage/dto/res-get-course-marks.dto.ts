import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResGetCoursetMarksDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  corsename: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  lectorname: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  studentname: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  studentsurname: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  mark: number;
}
