import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResGetStudentMarksDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  corsename: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  mark: number;
}
