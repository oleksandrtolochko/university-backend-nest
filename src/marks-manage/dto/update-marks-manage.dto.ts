import { PartialType } from '@nestjs/swagger';
import { CreateMarksManageDto } from './create-marks-manage.dto';

export class UpdateMarksManageDto extends PartialType(CreateMarksManageDto) {}
