import { MigrationInterface, QueryRunner } from 'typeorm';

export class DeleteColumnIsCheckedFromResetTokenTable1693665735375
  implements MigrationInterface
{
  name = 'DeleteColumnIsCheckedFromResetTokenTable1693665735375';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "resetToken" DROP COLUMN "isChecked"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "resetToken" ADD "isChecked" boolean NOT NULL DEFAULT false`,
    );
  }
}
