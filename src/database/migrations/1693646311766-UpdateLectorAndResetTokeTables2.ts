import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateLectorAndResetTokeTables21693646311766
  implements MigrationInterface
{
  name = 'UpdateLectorAndResetTokeTables21693646311766';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "resetToken" ADD "lector_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "resetToken" ADD CONSTRAINT "UQ_b1f0a5e7d9364755a4f728afe5c" UNIQUE ("lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "resetToken" ADD CONSTRAINT "FK_b1f0a5e7d9364755a4f728afe5c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "resetToken" DROP CONSTRAINT "FK_b1f0a5e7d9364755a4f728afe5c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "resetToken" DROP CONSTRAINT "UQ_b1f0a5e7d9364755a4f728afe5c"`,
    );
    await queryRunner.query(`ALTER TABLE "resetToken" DROP COLUMN "lector_id"`);
  }
}
