import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddOptionsOnDeleteOnUpdateToLectorAndResetTokenTables1693666054304
  implements MigrationInterface
{
  name = 'AddOptionsOnDeleteOnUpdateToLectorAndResetTokenTables1693666054304';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" DROP CONSTRAINT "FK_cf137a100d4b64fd1ad12fd48d4"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectors" ADD CONSTRAINT "FK_cf137a100d4b64fd1ad12fd48d4" FOREIGN KEY ("resetToken_id") REFERENCES "resetToken"("id") ON DELETE SET NULL ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" DROP CONSTRAINT "FK_cf137a100d4b64fd1ad12fd48d4"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectors" ADD CONSTRAINT "FK_cf137a100d4b64fd1ad12fd48d4" FOREIGN KEY ("resetToken_id") REFERENCES "resetToken"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
