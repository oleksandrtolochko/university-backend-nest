import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddColuntIsCheckedToResetTokenTable1693654232612
  implements MigrationInterface
{
  name = 'AddColuntIsCheckedToResetTokenTable1693654232612';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "resetToken" ADD "isChecked" boolean NOT NULL DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "resetToken" DROP COLUMN "isChecked"`);
  }
}
