import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateLectorAndResetTokenTables31693646429589
  implements MigrationInterface
{
  name = 'UpdateLectorAndResetTokenTables31693646429589';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "resetToken" DROP CONSTRAINT "FK_b1f0a5e7d9364755a4f728afe5c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "resetToken" DROP CONSTRAINT "UQ_b1f0a5e7d9364755a4f728afe5c"`,
    );
    await queryRunner.query(`ALTER TABLE "resetToken" DROP COLUMN "lector_id"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "resetToken" ADD "lector_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "resetToken" ADD CONSTRAINT "UQ_b1f0a5e7d9364755a4f728afe5c" UNIQUE ("lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "resetToken" ADD CONSTRAINT "FK_b1f0a5e7d9364755a4f728afe5c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
