import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateLectorTable1693645509176 implements MigrationInterface {
  name = 'UpdateLectorTable1693645509176';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" DROP CONSTRAINT "FK_d7ddb6a8416ce734535917cf24e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectors" RENAME COLUMN "resetTokenId" TO "resetToken_id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectors" ADD CONSTRAINT "FK_cf137a100d4b64fd1ad12fd48d4" FOREIGN KEY ("resetToken_id") REFERENCES "resetToken"("id") ON DELETE NO ACTION ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" DROP CONSTRAINT "FK_cf137a100d4b64fd1ad12fd48d4"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectors" RENAME COLUMN "resetToken_id" TO "resetTokenId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lectors" ADD CONSTRAINT "FK_d7ddb6a8416ce734535917cf24e" FOREIGN KEY ("resetTokenId") REFERENCES "resetToken"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
