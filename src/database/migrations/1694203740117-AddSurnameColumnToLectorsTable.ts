import { MigrationInterface, QueryRunner } from "typeorm";

export class AddSurnameColumnToLectorsTable1694203740117 implements MigrationInterface {
    name = 'AddSurnameColumnToLectorsTable1694203740117'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lectors" ADD "surname" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lectors" DROP COLUMN "surname"`);
    }

}
