import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ResetToken } from './entities/reset-token.entity';
import { LectorsService } from '../lectors/lectors.service';
import { Lector } from 'src/lectors/entities/lector.entity';
import { ResetPasswordWithTokenRequestDto } from '../auth/dto';
import { MailService } from '../mail/mail.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class ResetTokenService {
  private logger: Logger;
  constructor(
    private readonly lectorsService: LectorsService,
    private mailService: MailService,

    @InjectRepository(ResetToken)
    private resetTokenRepository: Repository<ResetToken>,
    private jwtService: JwtService,
  ) {
    this.logger = new Logger(ResetTokenService.name);
  }

  public async generateResetToken(user: Lector) {
    const payload = { sub: user.id, username: user.email };

    const resetToken = await this.jwtService.signAsync(payload);

    const newToken: ResetToken = new ResetToken();
    newToken.lector = user;
    newToken.resetToken = resetToken;

    const res = await this.resetTokenRepository.save(newToken);

    if (!res) {
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    await this.mailService.sendUserConfirmation(user.email, resetToken);

    return {
      message:
        'Token successfully created check your email. You have 5 minutes for resetting your password',
    };
  }

  public async checkResetToken(token: string) {
    const existToken: ResetToken | null =
      await this.resetTokenRepository.findOneBy({ resetToken: token });

    if (!existToken) {
      throw new HttpException(
        "There are wasn't request to the changing password",
        HttpStatus.NOT_FOUND,
      );
    }

    return { message: 'Go to the next step' };
  }

  public async resetPassword(
    resetPasswordDto: ResetPasswordWithTokenRequestDto,
  ) {
    const { token, newPassword } = resetPasswordDto;

    const payload = this.jwtService.decode(token) as {
      [key: string]: any;
    } | null;

    if (!payload || typeof payload !== 'object') {
      throw new HttpException('Invalid token', HttpStatus.BAD_REQUEST);
    }
    const { username } = payload;

    const user = await this.lectorsService.findOneLectorByEmail(username);

    if (!user.resetToken) {
      throw new HttpException(
        `There is no request to change password for this user`,
        HttpStatus.BAD_REQUEST,
      );
    }
    const hashPass = await bcrypt.hash(newPassword, 10);
    user.password = hashPass;

    const res = await this.lectorsService.updateLector(user.id, user);

    if (!res) {
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
    await this.removeResetToken(token);

    return res;
  }

  public async removeResetToken(token: string) {
    const existToken = await this.resetTokenRepository.findOneBy({
      resetToken: token,
    });
    if (!existToken) {
      throw new HttpException(
        "There are wasn't request to the changing password",
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.resetTokenRepository.delete(existToken.id);
  }
}
