import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { ResetTokenService } from './reset-token.service';
import { LectorsModule } from '../lectors/lectors.module';
import { ResetToken } from './entities/reset-token.entity';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ResetToken]),
    LectorsModule,
    JwtModule.register({
      global: true,
      secret: process.env.SECRET_JWT,

      signOptions: { expiresIn: '300s' },
    }),
    MailModule,
  ],
  providers: [ResetTokenService],
  exports: [ResetTokenService],
})
export class ResetTokenModule {}
