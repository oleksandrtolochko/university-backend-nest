import { Column, Entity, OneToOne } from 'typeorm';
import { CoreEntity } from '../../entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'resetToken' })
export class ResetToken extends CoreEntity {
  @Column({
    nullable: false,
    type: 'varchar',
  })
  resetToken: string;

  @OneToOne(() => Lector, (lector) => lector.resetToken, {
    onDelete: 'SET NULL',
    onUpdate: 'CASCADE',
  })
  lector: Lector;
}
